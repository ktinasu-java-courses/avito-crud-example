package org.example;

import lombok.extern.slf4j.Slf4j;
import org.example.data.Apartment;
import org.example.data.SearchQuery;
import org.example.manager.AvitoManager;

import java.util.List;

@Slf4j
public class AvitoCrudExample {
    public static void main(String[] args) {
        final Apartment firstApartment = new Apartment(0, true, false, 2_500_000, 23.5, false, true, 5, 9, "address1", "description1");
        final Apartment secondApartment = new Apartment(2, false, false, 4_000_000, 45, true, false, 3, 3, "address2", "description2");
        final Apartment thirdApartment = new Apartment(0, false, true, 1_560_000, 56.7, false, false, 1, 9, "address3", "description3");
        final Apartment fourthApartment = new Apartment(5, false, false, 5_500_000, 78.5, true, true, 9, 10, "address4", "description4");
        final Apartment fifthApartment = new Apartment(0, false, true, 3_000_000, 55, false, true, 1, 9, "address5", "description5");
        final Apartment sixthApartment = new Apartment(4, false, false, 4_500_000, 43.5, true, false, 5, 5, "address6", "description6");

        final AvitoManager manager = new AvitoManager();

        manager.create(firstApartment);
        manager.create(secondApartment);
        manager.create(thirdApartment);
        manager.create(fourthApartment);
        manager.create(fifthApartment);
        manager.create(sixthApartment);

        final List<Apartment> all = manager.getAll();

        final int managerCount = manager.getCount();
        log.debug("The number of apartments is: {}", managerCount);
        final Apartment managerById = manager.getById(3);
        log.debug("Apartment with id {}: {}", 3, managerById);

        final SearchQuery firstSearch = new SearchQuery(false, false, 2, 4, 3_000_000, 4_200_000, 40, 60, true, false, 2, 7, true, false, 1, 9);
        final List<Apartment> firstResults = manager.search(firstSearch);
        log.debug("Results for first query: {}", firstResults);

        final SearchQuery secondSearch = new SearchQuery(false, false, 1, 5, 3_900_000, 4_600_000, 40, 46, true, false, 3, 5, true, false, 1, 7);
        final List<Apartment> secondResults = manager.search(secondSearch);
        log.debug("Results for second query: {}", secondResults);

        manager.removeById(4);

        firstApartment.setPrice(2_700_000);
        manager.update(firstApartment);
        log.debug("Updation: id - {}, price - {} ", firstApartment.getId(), firstApartment.getPrice());
    }
}
